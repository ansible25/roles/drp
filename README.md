drp
=========

An Ansible role that installs Digital Rebar Provision (drp) server

Requirements
------------

NA

Role Variables
--------------
In defaults/main.yml

* drp_version: Version of drp to install (I.E. 4.6.2)
* drp_zip: Name of zip file to use as source of install (I.E. dr-provision.zip)
* drp_user: Name of user to configure for use in drp but is not a Linux system user (I.E. bob)
* drp_password: Password to use for the above user. You should change this to something better. (I.E. password)

Dependencies
------------

NA

Example Playbook
----------------

Applying role to hosts in the `servers` group

    - hosts: servers
      roles:
         - drp

License
-------

MIT

Author Information
------------------

This role was created by [bradthebuilder](https://bradthebuilder.me)
